from openerp.osv import osv, fields

class tour_booking(osv.osv):
    _name = 'tour.booking'
    _columns = {
            'state':fields.selection([
                ('draft','Draft'),
                ('confirmed','Confirmed'),
                ('in_process','In Process'),
                ('booked','Booked'),
                ('invoiced','Invoiced'),
                ('done','Done'),
                ('cancel','Cancel'),
                 ],    'Status', select=True, readonly=True),
            'name': fields.char('Tour Booking ID', readonly=True),
            'current_date': fields.date('Booking Date'),
            'customer_id':fields.many2one('res.partner', 'Customer', required=True),
            'email_id':fields.char('Email'),
            'mobile1':fields.char('Mobile Number', required=True),
            'adult': fields.integer('Adult'),
            'child': fields.integer('Child'),
            'via':fields.selection([
                ('direct','Direct'),
                ('agent','Agent'),
                 ],    'Via', required=True),
            'pricelist_id':fields.many2one('product.pricelist', 'Pricelist', required=True),
            'tour_type':fields.selection([
                ('international','International'),
                ('domestic','Domestic'),
                 ],    'Tour Type', required=True),
            'season_id':fields.many2one('tour.season', 'Season', required=True),
            'tour_id':fields.many2one('tour.package', 'Tour', required=True),
            'book_seat':fields.boolean('Is Book Seat'),
            'tour_dates_id':fields.many2one('tour.dates', 'Tour Dates', required=True), 
            'payment_policy_id':fields.many2one('tour.payment.policy', 'Payment Policy', required=True),
            'tour_customers_ids':fields.one2many('tour.customer.details', 'tour_book_id', 'Tour Customers'), 
            'insurance_line_ids':fields.one2many('tour.insurance.line', 'tour_book_id2', 'Insurance Lines'), 
            'tour_sale_order_ids':fields.one2many('sale.order', 'tour_book_id', 'Sale Orders'),
            'tour_account_invoice_ids':fields.one2many('account.invoice', 'tour_book_id', 'Account Invoices'),  
            'tour_cost': fields.float("Tour Cost"),
            'total_insurance_amt': fields.float("Insurance Amount"),
            'total_amt': fields.float("Total Amount"), 
         }
    _defaults = {  
        'state':'draft',
        'book_seat': False,
        }
    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        data_obj = self.pool.get('ir.model.data')
        sequence_ids = data_obj.search(cr, uid, [('name','=','seq_tour_booking')], context=context)
        sequence_id = data_obj.browse(cr, uid, sequence_ids[0], context=context).res_id
        if vals.get('name','/') =='/':
            code = self.pool.get('ir.sequence').get_id(cr, uid, sequence_id, 'id', context) or '/'
            vals['name'] = code
        return super(tour_booking, self).create(cr, uid, vals, context=context)
    
    def check_data(self,cr,uid,ids,context):
        record = self.browse(cr,uid,ids,context)
        if record.tour_customers_ids:
                customer_object = self.pool.get('tour.customer.details')
                adult_cus = len(customer_object.search(cr,uid,[('type', '=', 'adult'),('tour_book_id','=',ids[0])]))
                print "---------------------",adult_cus
                child_cus = len(customer_object.search(cr,uid,[('type', '=', 'child'),('tour_book_id','=',ids[0])]))
                ifa = ((adult_cus != record.adult) or (child_cus != record.child))
                if ifa:
                    print"---------------------",ifa
                    raise osv.except_osv(('Error!'), ('Customer details are not proper.'))
        else :    
                raise osv.except_osv(('Error!'), ('There is no customer in the list'))
              
        return self.write(cr, uid, ids, {'state': 'confirmed'}, context)
    
    def tour_cancel(self,cr,uid,ids,context):
        return self.write(cr,uid,ids,{'state':'cancel'},context)
    
    def create_order(self,cr,uid,ids,context):
        sale_obj=self.pool.get('sale.order')
        current_record=self.browse(cr,uid,ids,context)
        so_values={
                   'partner_id' : current_record.customer_id.id,
                   'order_policy' : 'prepaid',
                   'tour_book_id' : ids[0],
                   'origin' : current_record.name,
                   }
        sale_order_id = sale_obj.create(cr,uid,so_values)
        line = {
                'product_id' : current_record.tour_id.product_id.id,
                'name' : current_record.tour_id.product_id.name,
                'prod_uom_qty' : 1,
                'price_unit' : current_record.tour_id.product_id.list_price,
                'order_id':sale_order_id,
                }
        self.pool.get('sale.order.line').create(cr,uid,line)
        return self.write(cr, uid, ids, {'state': 'in_process'}, context)
    
    def confirm_booking(self,cr,uid,ids,context):
        record = self.browse(cr,uid,ids,context)
        if record.tour_sale_order_ids:
            for order in record.tour_sale_order_ids:
                if order.state == 'draft' : raise osv.except_osv(('Error!'), ('Please confirm sale order.'))
        if record.tour_account_invoice_ids:
            for order in record.tour_account_invoice_ids:
                if order.state == 'draft' : raise osv.except_osv(('Error!'), ('Please validate the invoice.'))
        return self.write(cr, uid, ids, {'state': 'booked','book_seat':True}, context)
    
    def check_payment(self,cr,uid,ids,context):
        not_payed_ids = self.pool.get('account.invoice').search(cr,uid,[('state', '!=', ['paid','cancel']),('tour_book_id','=',ids[0])])
        if not_payed_ids : raise osv.except_osv(('Error!'), ('Please pay the invoice amount.'))
        else : return self.write(cr, uid, ids, {'state': 'invoiced'}, context)
        
    def action_done(self,cr,uid,ids,context):
        return self.write(cr, uid, ids, {'state': 'done'}, context)
        
class tour_customer_details(osv.osv):
    _name = 'tour.customer.details'
    _columns = {
            'partner_id':fields.many2one('res.partner', 'Customer'),
            'name': fields.char('Age' , required=True),
            'gender':fields.selection([
                ('male','Male'),
                ('female','Female'),
                 ],    'Gender' , select=True),
            'type':fields.selection([
                ('adult','Adult'),
                ('child','Child'),
                 ],    'Adult/Child', select=True),
            'h_flag':fields.boolean('H' , readonly=True),
            't_flag':fields.boolean('T' , readonly=True),
            'i_flag':fields.boolean('I' , readonly=True),
            'v_flag':fields.boolean('V' , readonly=True),
            'p_flag':fields.boolean('P' ,readonly=True),
            'state':fields.selection([
                ('confirm','Confirmed'),
                ('cancel','Cancelled'),
                 ],    'Status', select=True),
            'tour_book_id':fields.many2one('tour.booking', 'Tour Book'),
         }

class tour_insurance_line(osv.osv):
    _name = 'tour.insurance.line'
    _columns = { 
                'insurance_policy_id':fields.many2one('insurance.policy', 'Insurance Policy', required=True),
                'name': fields.integer('Adult Policy Coverage'),
                'child_coverage1': fields.integer('Child Policy Coverage'),
                'insurance_cost': fields.float('Total Cost'),
                'tour_book_id2':fields.many2one('tour.booking', 'Tour Book'), 
         }

        
        
class tour_inherit_sale_order(osv.Model):
    _inherit = 'sale.order' 
    _columns = {
            'tour_book_id':fields.many2one('tour.booking', 'Tour Books'), 
                }
    
    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        if context is None:
            context = {}
        journal_id = self.pool['account.invoice'].default_get(cr, uid, ['journal_id'], context=context)['journal_id']
        if not journal_id:
            raise osv.except_osv(_('Error!'),
                _('Please define sales journal for this company: "%s" (id:%d).') % (order.company_id.name, order.company_id.id))
        invoice_vals = {
            'name': order.client_order_ref or '',
            'origin': order.name,
            'tour_book_id':order.tour_book_id.id,
            'type': 'out_invoice',
            'reference': order.client_order_ref or order.name,
            'account_id': order.partner_invoice_id.property_account_receivable.id,
            'partner_id': order.partner_invoice_id.id,
            'journal_id': journal_id,
            'invoice_line': [(6, 0, lines)],
            'currency_id': order.pricelist_id.currency_id.id,
            'comment': order.note,
            'payment_term': order.payment_term and order.payment_term.id or False,
            'fiscal_position': order.fiscal_position.id or order.partner_invoice_id.property_account_position.id,
            'date_invoice': context.get('date_invoice', False),
            'company_id': order.company_id.id,
            'user_id': order.user_id and order.user_id.id or False,
            'section_id' : order.section_id.id
        }

        invoice_vals.update(self._inv_get(cr, uid, order, context=context))
        return invoice_vals
    
class tour_inherit_account_invoice(osv.osv):
    
    _inherit = 'account.invoice'
    _columns = {
        'tour_book_id': fields.many2one('tour.booking','Tour Books'),
    }
    
class Partner(osv.Model):
    _inherit = 'res.partner'
    
    def _get_lines(self, cr, uid, ids, fields, args, context=None):
        line_obj = self.pool.get('tour.booking')
        res = {}
        for tour in self.browse(cr, uid, ids):
            line_ids = line_obj.search(cr, uid, [('book_seat', '=', True),('customer_id','=',ids[0])])
            res[tour.id] = line_ids
        return res
 
    def _set_lines(self, cr, uid, ids, name, value, inv_arg, context):
        line_obj = self.pool.get('tour.booking')
        for line in value:
            if line[0] == 1: # one2many Update
                line_id = line[1]
                line_obj.write(cr, uid, [line_id], line[2])
        return True

    _columns = {
                'is_agent':fields.boolean('Agent'),
                'is_hotel':fields.boolean('Hotel'),
                'data_ids': fields.function(_get_lines, fnct_inv=_set_lines, string='Tour Book', relation="tour.booking", method=True, type="one2many"),      
                   }
        
        
        
        
        