from openerp.osv import osv, fields

class tour_season(osv.osv):
    _name = 'tour.season'
    _columns = {
            'name':fields.char('Season'),
            'code':fields.char('Code'),
         }
    

class tour_pack_info(osv.osv):
    _name = 'tour.package.info'
    _columns = {
            'package_type_id':fields.many2one('tour.package.type', 'Package'), 
            'name':fields.selection([
                ('international','International'),
                ('domestic','Domestic'),
                 ],    'Tour Type', select=True),
            'tour_line_ids':fields.one2many('tour.package','tour_pack_info_id', 'Tour Lines'), 
         }
    
class tour_pack_type(osv.osv):
    _name = 'tour.package.type'
    _columns = {
            'name':fields.char('Tour Package'),
            'code':fields.char('Package Code'),
            'package_info_ids':fields.one2many('tour.package.info', 'package_type_id', 'Info Package'), 
         }

class tour_deduction_policy(osv.osv):
    _name = 'tour.deduction.policy'
    _columns = {
            'name':fields.integer('Minimum Limit (Days)'),
            'max_limit':fields.integer('Maximum Limit (Days)'),
            'deduction_percentage':fields.float('Deduction Percentage'), 
         }

class tour_facility(osv.osv):
    _name = 'tour.facility'
    _columns = {
            'name':fields.char('Facility Name'),
            'code':fields.char('Code',),
            'desc':fields.char('Description'), 
         }

class tour_destinations(osv.osv):
    _name = 'tour.destinations'
    _columns = {
            'name':fields.char('Destination Name'),
            'code':fields.char('Code'),
            'country_id':fields.many2one('res.country', 'Country'),
         }

class tour_payment_policy(osv.osv):
    _name = 'tour.payment.policy'
    _columns = {
            'name':fields.char('Policy Name'),
            'before_book_date_perc':fields.integer('Payment Percentage Before Booking Date'),
            'before_pay_date_perc':fields.integer('Payment Percentage After Booking Date'),
         }
    
    
    def onchange_book_date_percentage(self, cr, uid, ids, before_book_date_perc, context=None):
        if before_book_date_perc:
            before_pay_date_perc = 100 - before_book_date_perc
            return { 'value' : {'before_pay_date_perc' : before_pay_date_perc}}

