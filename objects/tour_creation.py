from openerp.osv import osv, fields

class tour_package(osv.osv):
    _name = 'tour.package'
    _defaults = {  
        'state':"draft",
    }
    
    def _get_lines(self, cr, uid, ids, fields, args, context=None):
        line_obj = self.pool.get('tour.booking')
        res = {}
        for tour in self.browse(cr, uid, ids):
            line_ids = line_obj.search(cr, uid, [('book_seat', '=', True),('tour_id','=',ids[0])])
            res[tour.id] = line_ids
        return res
 
    def _set_lines(self, cr, uid, ids, name, value, inv_arg, context):
        line_obj = self.pool.get('tour.booking')
        for line in value:
            if line[0] == 1: # one2many Update
                line_id = line[1]
                line_obj.write(cr, uid, [line_id], line[2])
        return True
    
    def check_data(self, cr, uid, ids, context=None):
        context = context or {}
        for o in self.browse(cr, uid, ids):
            if o.tour_dates_ids and o.tour_prog_ids and o.tour_destinations_ids and o.tour_facility_ex_ids and o.tour_facility_in_ids:
                return self.write(cr,uid,ids,{'state':'confirm'},context)
            else:
                raise osv.except_osv(('Error!'), ('Please Fill all data related to Tour.')) 
                
    def onchange_product_do(self, cr, uid, ids, product_id, context=None): 
        if product_id :
            product = self.pool.get('product.product').browse(cr, uid, product_id)
            return { 'value' : {'name' : product.name}}  
        
    _columns = {
            'state':fields.selection([
                ('draft', 'Draft'),
                ('confirm', 'Confirm'),
                ('checkout', 'Checkout'),
                ('cancel', 'Cancel'),
                ('done', 'Done'),
                 ], 'Status', readonly=True),
            'code':fields.char('Tour Code'),
            'name':fields.char('Name'),
            'product_id':fields.many2one('product.product', 'Tour Name', required="True"),
            'tour_type':fields.selection([
                ('international', 'International'),
                ('domestic', 'Domestic'),
                 ], 'Tour Type', required="True"),
            'current_date': fields.date('Date of Publish' , required="True"),
            'days':fields.selection([
                ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                ('5', '5'), ('6', '6'), ('7', '7'), ('8', '8'),
                ('9', '9'), ('10', '10'), ('11', '11'), ('12', '12'),
                ('13', '13'), ('14', '14'), ('15', '15'), ('16', '16'),
                ('17', '17'), ('18', '18'), ('19', '19'), ('20', '20'),
                ('21', '21'), ('22', '22'), ('23', '23'), ('24', '24'),
                ('25', '25'), ('26', '26'), ('27', '27'), ('28', '28'),
                ('29', '29'), ('30', '30'),
                 ], 'days', required="True"),
            'tour_intro': fields.text('Description'),
            'tour_pack_info_id':fields.many2one('tour.package.info','Tour Lines Info'),
            'tour_prog_ids':fields.one2many('tour.programme', 'tour_package_id', 'Tour Programme'),
            'tour_destinations_ids':fields.one2many('tour.destination.line', 'tour_package_id2', 'Tour Destinations'),
            'tour_travel_ids':fields.one2many('tour.road.travel', 'tour_package_id3', 'Tour Road Travel'),
            'tour_facility_ex_ids':fields.one2many('tour.cost.exclude.facility', 'tour_package_id4', 'Tour Facility Exclude'),
            'tour_facility_in_ids':fields.one2many('tour.cost.include.facility', 'tour_package_id5', 'Tour Facility Include'),
            'tour_hotel_info_ids':fields.one2many('tour.destination.hotel.line', 'tour_package_id6', 'Tour Hotel Info Line'),
            'tour_dates_ids':fields.one2many('tour.dates', 'tour_package_id7', 'Tour Dates'),
            'tour_booking_customer_ids': fields.function(_get_lines, fnct_inv=_set_lines, string='Tour Book', relation="tour.booking", method=True, type="one2many"),
         }   

class tour_programme(osv.osv):
    _name = 'tour.programme'
    _columns = {
            'day_number': fields.char('Days'),
            'desc': fields.text('Description'),
            'is_breakfast':fields.boolean('Breakfast'),
            'is_lunch':fields.boolean('Lunch'),
            'is_dinner':fields.boolean('Dinner'),
            'tour_package_id':fields.many2one('tour.package', 'Tour Package'), 
         }

class tour_destination_line(osv.osv):
    _name = 'tour.destination.line'
    _columns = {
            'destination_id':fields.many2one('tour.destinations', 'Destination', required=True), 
            'country_id':fields.many2one('res.country', 'Country', required=True), 
            'name':fields.integer('No.Of Nights'),
            'is_visa':fields.boolean('Is Visa Required'),
            'visa_type':fields.selection([
                ('single','Tourist Visa (Single Entry)'),
                ('multiple','Tourist Visa (Multiple Entries)'),
                 ],    'Visa Type'),
            'tour_package_id2':fields.many2one('tour.package', 'Tour Package'),
            'hotel_lines':fields.one2many('tour.destination.hotel.line', 'tour_destination_id', 'Hotel Lines', required=True), 
         }


class tour_road_travel(osv.osv):
    _name = 'tour.road.travel'
    _columns = {
            'from_destination_id':fields.many2one('tour.destinations', 'From', required=True),
            'to_destination_id':fields.many2one('tour.destinations', 'To', required=True),
            'transport_type_id':fields.many2one('product.product', 'Transport Type', required=True),
            'travel_class_id':fields.many2one('travel.class', 'Travel Class', required=False),   
            'name':fields.integer('Distance in KM', required=True),
            'approx_time':fields.float('Time in Hours' ,required=True),
            'tour_package_id3':fields.many2one('tour.package', 'Tour Package'),
            'provider_ids':fields.one2many('provider.information.line', 'tour_road_travel_id', 'Providers', required=True),  
         }


class provider_information_line(osv.osv):
    _name = 'provider.information.line'
    _columns = {
            'provider_id':fields.many2one('transport.information', 'Service Provider', required=True),
            'transport_carrier_id':fields.many2one('transport.carrier', 'Carrier Name', required=True),
            'name':fields.boolean('Primary'),
            'tour_road_travel_id':fields.many2one('tour.road.travel', 'Tour Travels'), 
         }


class tour_cost_exclude_facility(osv.osv):
    _name = 'tour.cost.exclude.facility'
    _columns = {
            'facility': fields.many2one('tour.facility' ,'Facility'),
            'desc': fields.char('Description'),
            'tour_package_id4':fields.many2one('tour.package', 'Tour Package'), 
         }
    def onchange_description (self, cr, uid, ids, facility , context=None):
        if facility :   
            record = self.pool.get("tour.facility").browse(cr,uid,facility)
            return { 'value' : {'desc' : record.desc}}



class tour_cost_include_facility(osv.osv):
    _name = 'tour.cost.include.facility'
    _columns = {
            'facility': fields.many2one('tour.facility' ,'Facility'),
            'desc': fields.char('Description'),
            'tour_package_id5':fields.many2one('tour.package', 'Tour Package'), 
         }
    
    def onchange_description (self, cr, uid, ids, facility , context=None):
        if facility :   
            record = self.pool.get("tour.facility").browse(cr,uid,facility)
            return { 'value' : {'desc' : record.desc}}


class tour_destination_hotel_line(osv.osv):
    _name = 'tour.destination.hotel.line'
    _columns = {
            'hotel_type_id':fields.many2one('hotel.type', 'Hotel Type', required=True),
            'hotel_id':fields.many2one('res.partner', 'Hotel', required=True),
            'room_type_id':fields.many2one('product.product', 'Room Type', required=True),   
            'name':fields.boolean('Primary'),
            'tour_package_id6':fields.many2one('tour.package', 'Tour Package'),
            'tour_destination_id':fields.many2one('tour.destination.line', 'Tour Destinations'),
         }
    
    def create(self, cr, uid, vals, context=None):
        if 'tour_destination_id' in vals:
            destination=self.pool.get('tour.destination.line').browse(cr,uid,vals['tour_destination_id'])
            vals['tour_package_id6']=destination.tour_package_id2.id
        return super(tour_destination_hotel_line, self).create(cr, uid, vals, context=context)
    

class tour_dates(osv.osv):
    _name = 'tour.dates'
    _columns = {
            'season_id':fields.many2one('tour.season', 'Season', required=True),
            'name':fields.char('Tour Date'), 
            'start_date': fields.date('Start Date', required=True),
            'book_date': fields.date('Last Date Of Booking', required=True),
            'due_date': fields.date('Payement Due Date', required=True),
            'total_seats': fields.integer('Total Seats', required=True),
            'available_seats': fields.integer('Available Seats', required=True),
            'adult_cost_price': fields.float('Adult Cost Per Person', required=True),
            'child_cost_price': fields.float('Child Cost Per Person', required=True),
            'state':fields.selection([
                ('draft', 'Draft'),
                ('available', 'Available'),
                ('closed', 'Closed'),
                 ], 'Status', readonly=True),
            'tour_package_id7':fields.many2one('tour.package', 'Tour Package'),
            'tour_id':fields.many2one('tour.package', 'Tour ID'), 
         }
        
    _defaults = {  
        'state': 'draft',  
        }
    
    def check_dates(self, cr, uid,ids, context=None):
        record = self.browse(cr,uid,ids,context)
        if record.due_date < record.book_date : raise osv.except_osv(('error'), ("Book Date must be less than  Due Date" ) )
        if record.start_date < record.book_date : raise osv.except_osv(('error'), ("Book Date must be less than Start Date" ) )
        if record.start_date < record.due_date : raise osv.except_osv(('error'), ("Due Date must be less than Start Date" ) )
        return self.write(cr, uid,ids, {'state': 'available'}, context)
    def check_status1(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'closed'}, context)
    def check_status2(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'draft'}, context)
    
class Product(osv.Model):
    _inherit = 'product.product'
    _columns = {
        'is_tour': fields.boolean('Is Tour'),
    }
    
class product_tour_category(osv.osv):
    _inherit = 'product.template' 
    _columns = { 
                }
    def _default_category(self, cr, uid, context=None):
        if context is None:
            context = {}
        if context.get('categ',False) and context.get('categ') == 'travel':
            mod_obj = self.pool.get('ir.model.data')
            ref = mod_obj.get_object_reference(
            cr, uid, 'tour_and_travel', 'tour_service_category')
            if ref:
                return ref[1]
        if context.get('categ',False) and context.get('categ') == 'road':
            mod_obj = self.pool.get('ir.model.data')
            ref = mod_obj.get_object_reference(
            cr, uid, 'tour_and_travel', 'travel_service_category')
            if ref:
                return ref[1]
        return super(product_tour_category,self)._default_category(cr,uid,context)
    

    
    _defaults = {  
        'categ_id':_default_category,
        }


