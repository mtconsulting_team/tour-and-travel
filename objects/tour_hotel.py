from openerp.osv import osv, fields

class hotel_type(osv.osv):
    _name = 'hotel.type'
    _columns = {
            'name': fields.char('Hotel Type' ,required="True"),
            'description': fields.char('Description',required="True"),
         }

class room_type(osv.osv):
    _name = 'room.type'
    _columns = {
                'room_type':fields.many2one('product.product', 'Room Type' , required="True"), 
                'name':fields.char('Description',required="True"), 
         }

class service_type(osv.osv):
    _name = 'service.type'
    _columns = {
                'service_id':fields.many2one('product.product', 'Service Type' , required="True"), 
                'name':fields.char('Description',required="True"), 
         }
