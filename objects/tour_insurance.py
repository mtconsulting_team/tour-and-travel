from openerp.osv import osv, fields

class insurance_type(osv.osv):
    _name = 'insurance.type'
    _columns = { 
                'name':fields.char('Name',required="True"), 
                'code':fields.char('Code',required="True"),
                'adult_cost': fields.float('For Adult Insurance Cost', required="True"),
                'child_cost': fields.float('For Child Insurance Cost', required="True"),
                  
                
         }
    
class insurance_policy(osv.osv):
    _name = 'insurance.policy'
    _columns = { 
                'insurance_type_id':fields.many2one('insurance.type', 'Insurance', required=True), 
                'name':fields.char('Insurance Name',required=True),
                'insurance_cost_for_adults': fields.float('Insurance Cost For Adult', required=True),
                'insurance_cost_for_childs': fields.float('Insurance Cost For Child', required=True),
                'coverage_lines_ids':fields.one2many('insurance.coverage.line', 'insurance_policy_id', 'Coverage Lines'),       
         }

class insurance_coverage_line(osv.osv):
    _name = 'insurance.coverage.line'
    _columns = { 
                'product_id':fields.many2one('product.product', 'Coverage', required=True), 
                'benifit_cost':fields.float('Benefit Cost',required=True),
                'insurance_policy_id':fields.many2one('insurance.policy', 'Insurance Policy'), 
                       
         }

