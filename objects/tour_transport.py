from openerp.osv import osv, fields

class transport_information(osv.osv):
    _name = 'transport.information'
    _columns = {
            'partner_id':fields.many2one('res.partner', 'Service Provider'), 
         }

class transport_carrier(osv.osv):
    _name = 'transport.carrier'
    _columns = {
                'name':fields.char('Travel Class',required="True"), 
                'code':fields.char('Travel Class Code'), 
         }

class travel_class(osv.osv):
    _name = 'travel.class'
    _columns = {
                'name':fields.char('Travel Class',required="True"), 
                'code':fields.char('Travel Class Code'), 
                'transport_type_id':fields.many2one('product.product', 'Transport Type', required="True"), 
         }
